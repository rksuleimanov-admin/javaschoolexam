package com.tsystems.javaschool.tasks.subsequence;

import java.util.LinkedList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        List resultList = new LinkedList();
        if (y == null || x == null) {
            throw  new IllegalArgumentException();
        }
        int temp = 0;
        for (int i = 0; i < x.size(); i++) {
            for (int j = temp; j < y.size(); j++) {
                if (x.get(i).toString().equals(y.get(j).toString())) {
                    if(i < x.size() - 1) {
                        i++;
                    }
                    resultList.add(y.get(j));
                    temp = j;
                }
            }
        } if (resultList.equals(x)) {
            return true;
        } else {
            return false;
        }
    }
}
