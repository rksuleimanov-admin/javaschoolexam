package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            if (!strValidation(statement)) return null;
            String res = infixToPostfix(statement);
            Double d = calcRPN(res);
            if (d == Math.floor(d)) {
                return Integer.toString(d.intValue());
            } else {
                return d.toString();
            }
        } catch (ArithmeticException e) {
            return null;
        }
    }

    private boolean strValidation(String input) {
        if (input == null || input.isEmpty()) return false;
        char[] chars = input.toCharArray();
        boolean opCheck = true, dotCheck = true;
        int parentheses = 0;
        for (int i = 0; i < input.length(); i++) {
            if (!isOperator(chars[i]) && !isDigit(chars[i]) && !isDelimiter(chars[i]) && chars[i] != ')' && chars[i] != '(') return false;
            if (chars[i] == ')') parentheses++;
            if (chars[i] == '(') parentheses--;
            if (isOperator(chars[i]) && !opCheck && (chars[i] != '(' && chars[i] != ')')) return false;
            if (isOperator(chars[i]) && (chars[i] != '(' && chars[i] != ')')) opCheck = false;
            if (!isOperator(chars[i]) || (chars[i] == '(' || chars[i] == ')')) opCheck = true;
            if (chars[i] == '.' && !dotCheck) return false;
            if (chars[i] == '.') dotCheck = false;
            if (chars[i] != '.') dotCheck = true;
        }
        return parentheses == 0;
    }

    private double calcRPN(String input) {
        char[] chars = input.toCharArray();
        Stack<Double> digStack = new Stack();
        for (int i = 0; i < chars.length; i++) {
            if (isDelimiter(chars[i])) {
                continue;
            }
            if (isDigit(chars[i])) {
                String temp = "";
                while (!isOperator(chars[i]) && !isDelimiter(chars[i])) {
                    temp += chars[i];
                    i++;
                    if (i == chars.length) break;
                }
                i--;
                digStack.push(Double.parseDouble(temp));
            }
            if (isOperator(chars[i])) {
                digStack.push(calculate(digStack.pop(), digStack.pop(), chars[i]));
            }
        }
        return digStack.pop();
    }

    private double calculate(double b, double a, char ch) {
        switch (ch) {
            case '/': {
                if (b == 0) throw new ArithmeticException();
                return a / b;
            }
            case '*':
                return a * b;
            case '+':
                return a + b;
            case '-':
                return a - b;
            default:
                return -1;
        }
    }

    private String infixToPostfix(String input) {
        char[] ch = input.toCharArray();

        Stack<Character> opStack = new Stack();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {

            if (isDelimiter(ch[i]))
                continue;

            if (isDigit(ch[i])) {
                while (!isDelimiter(ch[i]) && !isOperator(ch[i])) {

                    result.append(ch[i]);
                    i++;
                    if (i == input.length()) break;
                }
                result.append(" ");
                i--;
            } else if (ch[i] == '(') {
                opStack.push(ch[i]);
            } else if (ch[i] == ')') {
                char temp = opStack.pop();
                while (temp != '(') {
                    if (opStack.empty()) return null;
                    result.append(temp);
                    temp = opStack.pop();
                }
            } else if (isOperator(ch[i])) {
                if (opStack.empty()) opStack.push(ch[i]);
                else {
                    while (!opStack.empty() && (getPriority(ch[i]) <= getPriority(opStack.peek()))) {
                        result.append(opStack.pop());
                    }
                    opStack.push(ch[i]);
                }
            }
        }
        while (!opStack.empty()) {
            result.append(opStack.pop());
        }
        return result.toString();
    }


    private boolean isOperator(char ch) {
        return "+-/*()".indexOf(ch) != -1;
    }


    private boolean isDelimiter(char ch) {
        return " ".indexOf(ch) != -1;
    }

    private boolean isDigit(char ch) {
        return "1234567890.".indexOf(ch) != -1;
    }

    private int getPriority(char ch) {
        switch (ch) {
            case '/':
            case '*':
                return 4;
            case '(':
                return 0;
            case '+':
                return 2;
            case '-':
                return 3;
            case ')':
                return 1;
            default:
                throw new IllegalArgumentException();
        }
    }
}
